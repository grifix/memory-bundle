<?php

declare(strict_types=1);

namespace Grifix\MemoryBundle\Tests;

use Grifix\Memory\MemoryInterface;
use Grifix\Memory\SystemMemory;
use Grifix\MemoryBundle\GrifixMemoryBundle;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

final class MemoryBundleTest extends KernelTestCase
{
    protected static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    protected static function createKernel(array $options = []): KernelInterface
    {
        /**
         * @var TestKernel $kernel
         */
        $kernel = parent::createKernel($options);
        $kernel->addTestBundle(GrifixMemoryBundle::class);
        $kernel->handleOptions($options);

        return $kernel;
    }

    public function testItWorks(): void
    {
        $container = self::getContainer();

        $this->assertTrue($container->has(MemoryInterface::class));
        $service = $container->get(MemoryInterface::class);
        $this->assertInstanceOf(SystemMemory::class, $service);
        $service->getUsage();
    }
}
